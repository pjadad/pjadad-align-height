var alignHeight = (function($) {
	var _selectors = [];
	var _resizeTimeout;

	function _run() {
		_selectors.forEach(_setHeights);
	}

	function _setHeights(selector) {
		// Even out all the heights temporarly
		// and force elements in to their intended rows
		$(selector).css({
			height: '100px', // 100 is just a number
			overflow: 'hidden'
		});

		// Wait until css above is really rendered
		// (issue with bootstrap 3)
		setTimeout(function(selector) {
			var rows = {};

			// Group elements in rows by top offset
			$(selector).each(function() {
				var offsetTop = $(this).offset().top;
				if (rows[offsetTop] === void 0) {
					rows[offsetTop] = [];
				}

				rows[offsetTop].push(this);
			});

			// Remove height temporarly
			$(selector).css('height', '');

			// Calculate & set heights
			for (var offsetTop in rows) {
				if (rows.hasOwnProperty(offsetTop)) {
					var row = rows[offsetTop];

					if (row.length > 1) {
						var heights = row.map(function(el) {
							return $(el).height();
						});
						var max = Math.max.apply(null, heights);+

						// Set max height on all elements in this row
						$(row).css('height', max);
					}
				}
			}

			// Reset the overflow value
			$(selector).css('overflow', '');
		}, 0, selector); // Pass selector as an argument
	}

	function _resizeHandler() {
		clearTimeout(_resizeTimeout);
		_resizeTimeout = setTimeout(_run, 50); // Debounce 50 ms
	}

	// Public
	return function(selectors) {
		// Validate the selector
		if (selectors === void 0) {
			throw new Error('Invalid selector');
		}

		// Add selectors to global variable
		if (Array.isArray(selectors)) {
			_selectors = _selectors.concat(selectors);
		} else {
			_selectors.push(selectors);
		}

		// Handle resize event
		$(window).off('resize', _resizeHandler);
		$(window).on('resize', _resizeHandler);

		_run();
	};
})(jQuery);