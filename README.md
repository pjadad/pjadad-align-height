# PJADAD Align Height

Align any html elements height, based on their offset top

## Dependencies
- jQuery

## Usage

With one css selector:

alignHeight('p.my-class');

Or multiple css selectors:

alignHeight(['p', 'h2']);